ODOO及ODOO中国社区发展简史
=================================
1. 2002 年比利时的 13 岁开 始学习编程序的 Fabien Pinckaers 所创建创办了 Tiny Sprl 公 司。Tiny Sprl 公司的第一个产品就是开发 Tiny ERP，即后来的 OpenERP，现在改名为 Odoo。

#### 2005 TinyERP 1.0
#### 2006 TinyERP 2.0
#### 2007 TinyERP 3.0

1. 2007 年 OpenERP 由校长传播到中国，2007 年 7 月创建 shine-it.net 中文社区论坛，用的 是 004 年注册的公司上海先安信息的公司域名。论坛成立后没什么人，校长就把上海-Jeff 从 weberp 拉过来了，Jeff 还拉了个 simon 来，这家伙后来叫 jack 了。校长主要学习 python 和 OpenERP 开发技术。Jeff 主要关注应用。翻译的维护工作一直在 Jack 身上。
#### 2008 TinyERP 4.0

1. 2008 总监 shelly 和 oldrev 这样的高手是在这年出现的。
#### 2009 OpenERP 5.0

1. 2009 年上海-老肖的《OpenERP 开发和实施指南》，让 Odoo 中文社区论坛焕发了生机，初学者 有了迅速入门的信心，pdf 的方式比 wiki 更适合推广。

1. 2009 年初，四川-Toop 创建 Python 学习者交流群(现在的 Odoo 应用群，群号:212904)，人 很少，连 shelly 这个话痨那时候都找不到人交流。

1. 2009 年上海 Jeff-创建 OpenERP 开发群，并把老肖从 msn 群拉进了这个 qq 群，并和 shelly 一起激活了这个群，一直到现在一发不可收拾。

1. 2010 年 OpenERP 本地化探索---hornerp。目标是 OpenERP 本地化版本，中国人可以开包即用 的，含中文的安装包、中文的会计科目和报表打印、符合中国人习惯的进销存等。初期，项 目经理是老肖，技术由 oldrev 负责，需求由上海-jeff 负责。1.0 发布后，改由上海-jeff 任项目经理，后来 Joshua 出手相助，完成了 hornerp2.0。接下来的 3.0，因为项目范围极难 划定，需求无法文档化，而且项目团队越做越小，hornerp 项目就这样结束了。
#### 2011 OpenERP 6.0
1. 2011 年 OpenERP 的多家服务公司正式成立。king 和老肖成立了开源智造(OSCG) 
1. 2011 年 12 月，上海-Jeff 将保定-粉刷匠从 WebERP 拉到 OpenERP 里来。

#### 2012 OpenERP 6.1

1. 2012 年 3 月 Python 学习者交流群的管理员总监 sherlly 按粉刷匠的建议更名为 OpenERP 应 用群，并提升粉刷匠为群主，上海-Jeff 和南京 ccdos 在此群开始组织 OpenERP 的界面汉 化，这个群由当时的 10 几个人开始逐渐火起来了。

1. 2012 年，上海-Jeff 成立上海-开阖软件有限公司，专门提供 Odoo 实施和开发服务。

1. 2012 年 6 月保定粉刷匠在 OpenERP 应用群开始组织官方 OpenERP6.1 手册的翻译，

1. 2012 年 8 月 20 日，保定-粉刷匠组织的《OpenERP6.1 中文手册》正式开始在淘宝出售。

#### 2013 OpenERP 7.0

1. 2013 年，广州-步科 发布了 OpenERP7.0 绿色版，分为 Windows 和 Linux 两个版本，促进了 OpenERP 的推广和学习。

1. 2013年9月，上海对外经贸大学张国锋老师将Odoo引入到教学与科研中。

#### 2014 Odoo 8.0

 
1. OpenERP 更名为 Odoo , 坊间认为是 OnDemand OpenObject 的缩写，既‘按需定制的开放对象’

#### 2015 Odoo 9.0
#### 2016 Odoo 10.0


1. 2016 年 Odoo 的中国化版本 GoodERP 由上海开阖软件公司正式发布，同年开源智造开始推智 能云 Odoo 服务。

1. 2016 年起,odoo 在国内,已经遍地开发,呈百家争鸣之势.

1. 2016 年 11 月 5 日，以 “管理理论创新及商务软件的涅槃重生”为主题的第一届 Odoo 中国发展学术研讨会在上海对外经贸大学召开，来自全国各地 80 多名 Odoo 工程师、Odoo 用户、Odoo 爱好者及高校学者参加了会议。Odoo 的中国化版本 GoodERP 也由上海开阖软件公司正式发布，同年开源智造开始推广智能云 Odoo 服务。

1. 2016 年 11 月, odoo 大 V 社 www.odoov.com 由刘春波发起,全国各地 odoo 社区成员纷纷响应, 历时一年又 3 个月,完成丹尼尔的开发著作中文翻译工作.

#### 2017 Odoo 11.0


1. 2017 年 11 月 28 日，第二届 Odoo 中国发展学术研讨会将在上海对外经贸大学召开，100 余人参加会议。上海浦东国际金融学会、南京华信区块链研究院、工业 4.0 协会与 Odoo 工程师一道探讨了 Odoo 投融资、Odoo 与区块链结合、Odoo 与工业 4.0 结合的途径与模式。

1. 2017 年 jeffery 被 Odoo 官方提名 The best translator

#### 2018 Odoo 12.0

1. 2018 年 4 月 16 日，国内企业浪潮将与欧洲开源 ERP 软件提供商 Odoo 成立合资公司，浪潮 ERP 渠道总监徐翔宇将出任总经理，合资公司将基于 Odoo 开源模式推出开源 ERP 产品，满足 中小企业 SaaS 定制化的需求，并在五年内实现中小企业 SaaS 市场第一。

1. 2018 年 4 月份以后，大批社区大牛加盟浪潮。

1. 2018年6月，浪潮集团联合Odoo中国社区发起成立“中国开源工业PaaS分会”，挂靠中国开源软件推进联盟（COPU）。

1. 2018 年 7 月,odoo 中国化翻译组成立,组织完成 odoo10-12 版本主要翻译汉化;并整理,翻译 odoo 相关实施、技术开发资料,以纸质版发布.

1. 2018 年 10 月 27 日，由中国开源工业 PaaS 协会主办，浪潮和上海对外经贸大学承办的 2018 年中国开源工业 PaaS 技术与应用高峰论坛在上海召开。并为杰出人员颁奖.

1. 2018 年 11 月,由上海-Jeff 王剑峰组织并带领,各地积极响应 codesprint 活动.

1. 2018 年 12 月,由浪潮组织人力,对 odoo12 官方汉化进行全面 review.

1. 2018 年底,国内社区活跃人士及官方伙伴公司,积极发布开源工具模块.

1. 2018 年,由厦门碎石头,继续发布 odoo windows 绿色版,并不断更新源码,使绿色版能成为广 大开发与使用的便捷工具;

#### 2019 Odoo 13.0

1. 2019 年 1 月，浪潮集团、张国锋、章浩、王天扬、杨滨、于飞、许日、张杰做为代表，向上海市民政局正式提交注册“上海开源信息技术协会”。
1. 2019 年 1 月，上海-Alan 翻译并免费发布 Odoo12 开发手册。

1. 到 2019 年春节为止,以 odoo 为主要命名的 QQ 群已远超百个,千人群也超过 10 个. 

1. 2020 年 3 月 24 日，上海市民政局为“上海开源信息技术协会”颁发了法人证书。

#### 2020 Odoo 14.0

1. 2020 年 10 月，开源 ERP 系统 Odoo 第 14 版正式发布

#### 2021 Odoo 15.0

1. 2021 年 10 月，开源 ERP 系统 Odoo 第 15 版正式发布